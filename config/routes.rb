Rails.application.routes.draw do
  mount RedactorRails::Engine => '/redactor_rails'
  devise_for :users, :controllers => {
                       registrations: 'registrations',
                   :omniauth_callbacks => 'omniauth_callbacks'}

  resources :users do
    resources :posts
    resources :voices
    resources :revisions
    resources :collections
    resources :addresses
  end

  resources :orders do
    resources :cards
  end

  resources :cards do
    resources :addresses
    collection {post :import}
  end

  resources :rankings

  resources :task_lists do
    resources :tasks
  end

  resources :calendars
  resources :charges

  resources :sources, only: [:create, :destroy, :index]
  resources :collection_items, only: [:create, :destroy, :index]

  resources :users do
    member do
      get :following, :followers
    end
  end

  resources :messages do
    member do
      match 'mark_as_read', :to => 'messages#mark_as_read', via: :put
      match 'archive', :to => 'messages#archive', via: :put
      match 'mark_as_new', :to => 'messages#mark_as_new', via: :put
    end
  end
  resources :trends
  resources :users do
    member do
      match 'toggle' => 'users#toggle', via: :put
    end
  end

  resources :relationships, only: [:create, :destroy]

  match 'favorites', :to => 'favorites#index', via: :get

  resources :categories do
    resources :posts
    member do
      match 'upvote' => 'categories#upvote', via: :put
      match 'downvote' => 'categories#downvote', via: :put
      match 'publish' => 'categories#publish', via: :put
    end
  end

  match 'welcome', to: 'static_pages#welcome_registration', via: :get
  match 'analytics', to: 'static_pages#analytics', via: :get


  match 'manage', to: 'posts#manage', via: :get

  resources :posts do
    member do
      match 'upvote' => 'posts#upvote', via: :put
      match 'downvote' => 'posts#downvote', via: :put
      match 'submit', to: 'posts#submit', via: :put
      match 'revert', to: 'posts#revert', via: :put
      match 'approve', to: 'posts#approve', via: :put
      match 'ban', to: 'posts#ban', via: :put
    end



    resources :comments do
      member do
        match 'flag' => 'comments#flag', via: :put
      end
    end

    resources :revisions do
      member do
        patch :pass_off
      end
    end
  end

  match 'flag_comment', to: "comments#flag", via: :put

  resources :voices do
    member do
      match 'upvote' => 'voices#upvote', via: :put
      match 'downvote' => 'voices#downvote', via: :put
    end
  end

  resources :collections, only: [:create, :destroy, :index]

  get 'tags/:keyword', to: 'posts#index', as: :tag

  resources :forums do
    resources :topics
  end

  resources :topics do
    resources :submissions
  end

  resources :invitations

  match 'admin', to: 'admin#index', via: :get

  root 'static_pages#front_page'
end