module Stat
  def avg(model, up_down_or_flat)
    model.average(up_down_or_flat)
  end

  def the_weighted_average(model)
    model.average
  end

  def median_by_category(model)
    model.median(:cached_votes_up)
  end

  def max_votes(model)
    model.maximum(:cached_votes_count)
  end

  def min_votes(model)
    model.minimum(:cached_votes_count)
  end

  def range(model)
    model.maximum - model.minimum
  end

  def frequency
  end
end