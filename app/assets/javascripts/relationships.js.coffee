# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
Network = () ->
  width = 960
  height = 800
  # ...
  network = (selection, data) ->
    # main implementation

  update = () ->
    # private function

  network.toggleLayout = (newLayout) ->
    # public function

  return network

# The update() function performs the bulk of the
# work to setup our visualization based on the
# current layout/sort/filter.
#
# update() is called everytime a parameter changes
# and the network needs to be reset.
update = () ->
  # filter data to show based on current filter settings.
  curNodesData = filterNodes(allData.nodes)
  curLinksData = filterLinks(allData.links, curNodesData)

  # sort nodes based on current sort and update centers for
  # radial layout
  if layout == "radial"
    artists = sortedArtists(curNodesData, curLinksData)
    updateCenters(artists)

  # reset nodes in force layout
  force.nodes(curNodesData)

  # enter / exit for nodes
  updateNodes()

  # always show links in force layout
  if layout == "force"
    force.links(curLinksData)
    updateLinks()
  else
    # reset links so they do not interfere with
    # other layouts. updateLinks() will be called when
    # force is done animating.
    force.links([])
    # if present, remove them from svg
    if link
      link.data([]).exit().remove()
      link = null

  # start me up!
  force.start()
