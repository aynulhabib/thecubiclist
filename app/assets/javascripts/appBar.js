var appBar, metroJs;
$(document).ready(function(){
    appBar = $(".appbar").applicationBar({
        preloadAltBaseTheme: true,
        bindKeyboard: true,
        metroLightUrl: 'images/metroIcons_light.jpg',
        metroDarkUrl: 'images/metroIcons.jpg'
    });
    // append the theme options
    metroJs = jQuery.fn.metrojs;
    metroJs.theme.appendAccentColors({
        accentListContainer: ".theme-options"
    });
    metroJs.theme.appendBaseThemes({
        baseThemeListContainer: ".base-theme-options"
    });
});