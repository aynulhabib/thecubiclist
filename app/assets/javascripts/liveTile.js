$(document).ready(function(){
    var $tiles = $(".live-tile").liveTile({
        playOnHover:true,
        repeatCount: 0,
        delay: 0,
        initDelay: 0,
        startNow: false,
        animationComplete: function(tileData){
            $(this).liveTile("play");
            tileData.animationComplete = function(){};
        }
    }).each(function(idx, ele){
        var delay = idx * 1000;
        $(ele).liveTile("play", delay);
    });
});
