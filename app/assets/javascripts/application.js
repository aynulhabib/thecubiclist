// This is a manifest file that will be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//

//= require jquery
//= require jquery-ui
//= require jquery_ujs
//= require bootstrap.min
//= require jsapi
//= require typed
//= require chartkick
//= require redactor-rails
//= require turbolinks
//= require select2
//= require MetroJs.min
//= require list
//= require typeahead.bundle
//= require cocoon
//= require moment
//= require fullcalendar
//= require jquery.tokeninput
//= require_tree .



