class InvitationMailer < ActionMailer::Base
  default from: "aynul@thecubiclist.com"

  def invite_to_write(invitation)
    @invitation = invitation

    mail to: invitation.email,
         subject: invitation.subject

  end
end
