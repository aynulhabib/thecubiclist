class Comment < ActiveRecord::Base
  belongs_to :post
  belongs_to :user
  validates :body, length: {minimum: 1}
  validates :user_id, presence: true
  validates :post_id, presence: true
  delegate :pen_name, to: :user, prefix: true #-> allows you to call `@comment.user_email`
  acts_as_votable
end
