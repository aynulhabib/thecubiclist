class Voice < ActiveRecord::Base
  belongs_to :user
  acts_as_votable

  def author
    self.user.pen_name
  end
end
