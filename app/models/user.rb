require 'statistical'

class User < ActiveRecord::Base
  include Stat
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  searchkick word_start: [:name, :first_name, :last_name, :pen_name]
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :omniauthable, :omniauth_providers => [:facebook]
  belongs_to :contest
  has_many :posts, dependent: :destroy
  has_many :comments, dependent: :destroy
  has_many :revisions, dependent: :destroy
  has_many :submissions
  has_many :skills
  #friends
  has_many :active_relationships, class_name: 'Relationship', foreign_key: 'follower_id'
  has_many :passive_relationships, :class_name => 'Relationship', :foreign_key => 'followed_id'
  has_many :following, :through => :active_relationships, source: :followed
  has_many :followers, :through => :passive_relationships, :source => :follower
  #support
  has_many :voices
  has_many :collections
  has_many :collection_items
  #messages
  has_many :incoming_messages, class_name: 'Message', foreign_key: 'recipient_id'
  has_many :sent_messages, class_name: 'Message'
  has_many :clicks
  has_many :addresses, as: :addressable
  accepts_nested_attributes_for :addresses, :reject_if => :all_blank, :allow_destroy => true
  #ecommerce- buying
  has_one :cart, dependent: :destroy
  #ecommerce- selling
  has_many :stores, dependent: :destroy
  has_many :outbound_cards, class_name: 'Card', source: :sender
  has_many :inbound_cards, class_name: 'Card', source: :recipient
  has_many :orders, :as => :orderable
  #todo
  has_many :task_lists
  has_many :tasks, as: :taskable
  accepts_nested_attributes_for :orders, :reject_if => :all_blank, :allow_destroy => true
  mount_uploader :profile_picture, ProfilePictureUploader
  mount_uploader :cover_photo, CoverPhotoUploader
  acts_as_voter
  def facebook
    @facebook ||= Koala::Facebook::API.new(oauth_token)
  end
  def self.from_omniauth(auth)
    where(provider: auth.provider, uid: auth.uid).first_or_create do |user|
      user.email = auth.info.email
      user.password = Devise.friendly_token[0,20]
      user.name = auth.info.name   # assuming the user model has a name
      user.image = auth.info.image # assuming the user model has an image
    end
  end

  def self.new_with_session(params, session)
    super.tap do |user|
      if data = session["devise.facebook_data"] && session["devise.facebook_data"]["extra"]["raw_info"]
        user.email = data["email"] if user.email.blank?
      end
    end
  end

  def name
    return pen_name
  end

  def mailboxer_email(object)
    return self.email
  end

  def feed
  end

  def add_to_private_feed
  end

  def remove_from_private_feed
  end


#friend methods
  def follow(other_user)
    active_relationships.create(followed_id: other_user.id)
  end

  def unfollow(other_user)
    active_relationships.find_by(followed_id: other_user.id).destroy
  end

  def following?(other_user)
    following.include?(other_user)
  end

  def add_click(post)
    clicks.create(post_id: post)
  end

  def search_tokens=(ids)
    self.follower_ids = ids.split(",")
  end

  def full_name
    "#{self.first_name}#{self.last_name}"
  end

  def get_popularity
    self.posts.sum("cached_votes_score")
  end

  def get_score(category)
    self.posts.average(:cached_votes_score)
  end

  #my_top
  def my_top_10
    self.posts.order("cached_votes_score DESC").take(10)
  end

  #best posts
  def best_post
    self.posts.order("cached_votes_score DESC").first
  end

  def worst_post
    self.posts.order("cached_votes_score ASC").first
  end

  #my_total_votes
  def post_count
    self.posts.size
  end

  #my_total_upvotes
  def total_up
    self.posts.sum(:cached_votes_up)
  end

  #my_total_downvotes
  def total_down
    self.posts.sum(:cached_votes_down)
  end

  #my_average_score
  def avg_post_score
    self.posts.sum(:cached_votes_score)/(self.post_count)
  end

  #how I compare to the rest
  def rank
    Post.order("cached_votes_score DESC < ?", self.avg_post_score).count
  end

  def percentile
    (((self.rank)/(Post.count+1).to_f)*100).round(2)
  end

end
