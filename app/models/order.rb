class Order < ActiveRecord::Base
  belongs_to :orderable, polymorphic: true
  has_many :addresses, as: :addressable
  has_many :cards
end
