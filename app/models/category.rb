class Category < ActiveRecord::Base
  searchkick autocomplete: ['title']
  #associations
  belongs_to :parent_category, class_name: Category, foreign_key: 'parent_id'
  has_many :posts, dependent: :destroy
  #validations
  validates :name, presence: true
  validates :description, presence: true
  acts_as_votable
  mount_uploader :picture, CategoryPictureUploader
  include Workflow

  workflow do
    state :draft do
      event :publish, transitions_to: :published
    end

    state :published
  end


  def get_upvotes
    category.posts.get_upvotes.size
  end

  def as_json options={}
    {
        id: id,
        name: name
    }
  end
end


