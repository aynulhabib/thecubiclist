class Revision < ActiveRecord::Base
  belongs_to :post
  belongs_to :user
  has_many :tasks, as: :taskable
  validates :user_id, presence: true
  scope :sent_to_user, -> { where(:workflow_state => 'sent')}
  include Workflow
  workflow do
    state :unsent do
      event :send_to_user, :transitions_to => :sent
    end

    state :sent do
      event :complete_and_send_back_to_admin, :transitions_to => :completed_by_user
    end
  end
end
