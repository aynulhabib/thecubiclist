require 'statistical'

class Post < ActiveRecord::Base
  include Stat
  searchkick autocomplete: ['title']
  acts_as_votable
  acts_as_taggable
  belongs_to :user
  belongs_to :category
  #virtual attributes
  #before save callback
  has_many :comments, dependent: :destroy
  has_many :revisions, dependent: :destroy
  has_many :clicks
  has_many :sources, :as => :sourceable
  accepts_nested_attributes_for :sources, :reject_if => :all_blank, :allow_destroy => true
  validates :user_id, presence: true
  validates :title, presence: true
  validates :category_name, presence: true
  validates :brief, presence: true, length: { in: 20..100 }
  validates :content, presence: true
  validates :post_picture, presence: true
  scope :drafts, -> { where(workflow_state: 'draft') }
  scope :pending_approval, -> { where(workflow_state: 'pending_approval') }
  scope :approved, -> { where(workflow_state: 'published') }
  scope :needs_revision, -> { where(workflow_state: 'needs_revision') }
  scope :banned, -> { where(workflow_state: 'banned') }
  mount_uploader :post_picture, PostPictureUploader
  include Workflow

  def self.with_state(state)
    self.where(workflow_state: state)
  end

  workflow do
    state :draft do
      event :submit, :transitions_to => :pending_approval
    end

    state :pending_approval do
      event :approve, :transitions_to => :published
      event :revision_area, :transitions_to => :needs_revision
      event :ban, :transitions_to => :banned
      event :revert, :transitions_to => :draft
    end

    state :needs_revision do
      event :resubmit, :transitions_to => :revision_pending_approval
      event :approve, :transitions_to => :published
      event :revision_area, :transitions_to => :needs_revision
      event :ban, :transitions_to => :banned
    end

    state :revision_pending_approval do
      event :approve, :transitions_to => :published
      event :revision_area, :transitions_to => :needs_revision
      event :ban, :transitions_to => :banned
    end
    state :published
    state :banned
  end

  def author
    self.user.pen_name
  end

  #getter method
  def category_name
    category.try(:name)
  end

  def category_name=(name)
    self.category = Category.where(:name => name).first_or_create!
  end

  def self.impressions
    self.clicks.count
  end

  def self.avg_up
    self.sum(:cached_votes_up)/(self.count).to_f
  end

  def self.avg_down
    self.sum(:cached_votes_down)/(self.count).to_f
  end

  def self.avg_score
    self.sum(:cached_votes_score)/(self.count).to_f
  end

  def self.popular
    self.order('cached_votes_total DESC').first
  end

  def self.best
    self.order('cached_votes_score DESC').first
  end

  def self.worst
    self.order('cached_votes_score DESC').last
  end

  def self.rank(post)
    self.order('cached_votes_score < ?, post.cached_votes_score').count
  end

  def max_votes
  end

  def min_votes(model)
    model.minimum(:cached_votes_count)
  end

  def range(model)
    model.maximum - model.minimum
  end

  def drafts
    self.with_draft_state
  end

  def keyword_tokens=(ids)
    self.keyword_ids = ids.split(",")
  end

end
