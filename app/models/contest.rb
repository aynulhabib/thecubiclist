class Contest < ActiveRecord::Base
  has_many :participants, class_name: 'User', foreign_key: :contest_id
  has_many :leaderboards
end
