class Card < ActiveRecord::Base
  belongs_to :sender, class_name: 'User', foreign_key: :sender_id
  belongs_to :recipient, class_name: 'User', foreign_key: :recipient_id
  belongs_to :store
  mount_uploader :front, CardBackUploader
  mount_uploader :back, CardFrontUploader
  has_many :orders, as: :orderable
  acts_as_votable
  include Workflow

  workflow do
    state :draft do
      event :publish, :transitions_to => :published
    end
    state :published do
    end
  end

  def going_to
    nil ? self.recipient.name : 'User does not have a name'
  end
end
