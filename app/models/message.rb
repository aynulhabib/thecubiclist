class Message < ActiveRecord::Base
  scope :unread, -> { where(:workflow_state => 'unread')}
  scope :read, -> { where(:workflow_state => 'read')}
  scope :archived, -> { where(:workflow_state => 'archived')}
  scope :archived, -> { where(:workflow_state => 'archived')}
  belongs_to :user
  include Workflow

  workflow do
    state :unread do
      event :mark_as_read, :transitions_to => :read
      event :archive, :transitions_to => :archived
    end

    state :read do
      event :mark_as_new, :transitions_to => :unread
      event :archive, :transitions_to => :archived
    end

    state :archived do
      event :mark_as_new, :transitions_to => :unread
    end
  end


  def author()
    self.user.pen_name
  end

  def recipient()
    User.find(self.recipient_id)
  end

  def self.latest_messages
    order('created_at desc').first
  end

end
