class RevisionsController < ApplicationController
  before_action :set_post, except: [:index]
  before_action :set_user, except: [:create, :index]
  before_action :set_revision, except: [:create, :index]

  def index
    @revisions = Revision.all
  end

  def create
    @revision = @user.revisions.create(revision_params)
    set_post
    if @revision.save
      redirect_to @post
      flash[:success] = "Revision was saved!"
    else
      flash[:success] = "There was a mistake"
    end
  end

  def destroy
    if @revision.destroy
      flash[:success] = "Revision was deleted"
    else
      flash[:error] = "Revision could not be deleted"
    end
    redirect_to :back
  end

  def pass_off
    @revision.update_attribute(:workflow_state, "sent")
    redirect_to :back, notice: "Revision has been sent!"
  end

  def mark_as_complete
    @revision.update_attribute(:workflow_state, "completed")
    redirect_to :back, notice: "Revision has been marked as completed"
  end


  private

  def set_post
    @post = Post.find(params[:post_id])
  end

  def set_user
    @user = User.find(@post.user_id)
  end

  def set_revision
    @revision = @post.revisions.find(params[:id])
  end

  def revision_params
    params.require(:revision).permit(:title, :workflow_state, :user_id, :post_id)
  end

  def check_if_admin
    current_user.admin?
  end
end