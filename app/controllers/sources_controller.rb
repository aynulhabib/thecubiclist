class SourcesController < ApplicationController
  helper_method :sort_column, :sort_direction
  before_action :find_post, only: [:destroy, :create]


  def index
    @sources = Source.all
  end

  def create
    @source = @post.sources.create(source_params)
    if @source.save
      flash[:success] = 'Thanks! Your suggestion has been successfully added to the queue'
      redirect_to :back
    else
      flash[:error] = 'There was an error'
      redirect_to :back
    end
  end

  def destroy
    @source = Source.destroy
    if @source.destroy
      flash[:success] = 'Source has been destroyed'
    else
      flash[:error] = 'There was an error'
    end
  end

  private
  def source_params
    params.require(:source).permit(:name, :url, :thumb, :description, :type)
  end

  def find_source
    @source = Source.find(params[:id])
  end
end