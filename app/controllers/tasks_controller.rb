class TasksController < ApplicationController
  before_action :check_if_admin, only: [:index]
  before_action :set_task, only: [:destroy]

  def index
    @tasks = Task.all
  end

  def create
    @task = @revision.tasks.create(task_params)
    if @task.save
    else
      flash[:error] = "uh oh"
    end
    redirect_to @revision
  end

  def destroy
    @task.destroy
    redirect_to :back
  end


  private


  def set_task
    @task = Task.find(params[:id])
  end


  def set_task_list
    @task_list = TaskList.find(params[:id])
  end

  def task_params
    params.require(:task).permit(:content, :revision_id, :done)
  end

  def check_if_admin
    current_user.admin?
  end
end
