class CollectionsController < ApplicationController
    before_action :set_collection, only: [:destroy, :upvote, :downvote]

    def index
      @collections = current_user.collections
      @collection_info = "Organize, don't agonize. - Nancy Pelosi"
    end

    def create
      @collection = current_user.collections.create(collection_params)
      if @collection.save
        flash[:success] = 'Collection has been added'
        redirect_to user_collections_path
      else
        flash[:error] = 'There was an error'
        redirect_to user_collections_path
      end
    end

    def destroy
      @collection = Collection.destroy
      if @collection.destroy
        flash[:success] = 'Collection has been destroyed'
      else
        flash[:error] = 'There was an error'
      end
    end
private
  def collection_params
    params.require(:collection).permit(:name)
  end

end
