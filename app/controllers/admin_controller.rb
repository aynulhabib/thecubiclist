class AdminController < ApplicationController

def index
  @posts = Post.all
  @categories = Category.all
  @comments = Comment.all
  @users = User.all
  @drafts = Post.with_state(:draft)
  @pending_approval = Post.with_state(:pending_approval)
  @approved = Post.with_state(:published)
  @needs_revision = Post.with_state(:needs_revision)
  @banned = Post.with_state(:banned)
  @revisions_pending_approval = Post.with_state(:revisions_pending_approval)
  @revisions = Revision.all
  @sources = Source.all
  @voices = Voice.all
  @cards = Card.all
end



private
    def check_if_admin
     if current_user.admin?
     else
       redirect_to root_path
     end
    end


end
