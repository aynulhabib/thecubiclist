class RelationshipsController < ApplicationController

  def index
    @relationships = Relationship.each do |user|

    end
  end

  def create
    user = User.find(params[:followed_id])
    current_user.follow(user)
    redirect_to user
    flash[:success] = "Success! You are now following #{user.pen_name}"
  end

  def destroy
    user = Relationship.find(params[:id]).followed
    current_user.unfollow(user)
    redirect_to user
    flash[:success] = "You left your friendship with #{user.pen_name}."
  end
end