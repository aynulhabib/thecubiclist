class AddressesController < ApplicationController
  before_action :set_address, only: [:destroy, :upvote, :downvote]

  def index
    @my_address = current_user.addresses
    @address = Address.all
  end

  def create
    @address = current_user.address.create(address_params)
    if @address.save
      flash[:success] = 'Thanks! Your suggestion has been successfully added to the queue'
      redirect_to user_address_path
    else
      flash[:error] = 'There was an error'
      redirect_to user_address_path
    end
  end

  def destroy
    @address = Address.destroy
    if @address.destroy
      flash[:success] = 'Address has been destroyed'
    else
      flash[:error] = 'There was an error'
    end
  end
  

  private
  def address_params
    params.require(:address).permit(:address_line_1, :address_line_2, :address_city, :address_state, :address_zip, :address_country)
  end

  def set_address
    @address = Address.find(params[:id])
  end
end
