class PostsController < ApplicationController
  before_action :authenticate_user!, only: [:edit, :update, :destroy, :upvote, :downvote]
  before_action :find_post, except: [:new, :create, :manage, :index]
  before_action :correct_user, only: [:edit, :destroy, :manage]

  def new
    @post = current_user.posts.new
  end

  def create
    @post = current_user.posts.create(post_params)
    @post.save
    if params[:commit] == 'save as draft'
      redirect_to manage_path
      flash[:success] = "Saved as a draft"
    elsif params[:commit] == 'submit for approval'
      @post.submit!
      redirect_to manage_path
      flash[:notice] = "Post successfully submitted for approval"
    else
      flash[:notice] = "Oops, there was an error"
      redirect_to 'new'
    end
  end

  def show
    current_user.add_click(@post)
    @revisions = @post.revisions
    @sources = @post.sources
  end

  def index
    if params[:tag]
      @posts = Post.tagged_with(params[:tag])
    elsif params[:query]
      @posts = Post.search(params[:query], page: params[:page])
    else
      @posts = Post.all
    end
  end

  def edit
  end

  def update
    if @post.update_attributes(post_params) then
      redirect_to manage_path
      flash[:success] = "Draft has been successfully updated."
    else
      render 'edit'
    end
  end

  def destroy
    if @post.destroy && !current_user.admin?
      flash[:success] = "Post has been successfully deleted"
      redirect_to manage_path
    else
      redirect_to admin_path
      flash[:success] = "Post has been successfully deleted"
    end
  end

  def tag_cloud
    @tags = Post.tag_counts_on(:tags)
  end

  def toggle_favorite
    @post.toggle(:favorited)
  end

  def upvote
    @post.upvote_by current_user

    if request.xhr?
      render json: { count: @post.get_likes.size, id: params[:id]}
    end
  end

  def downvote
    @post.downvote_by current_user

    if request.xhr?
      render json: { count: @post.get_likes.size, id: params[:id]}
    end
  end

  def approve
    @post.approve!
    redirect_to :back
    flash[:notice] = "Post Accepted & Published"
  end

  def submit
    @post.submit!
    redirect_to :back
    flash[:notice] = "Post has been successfully submitted (pending review)"
  end

  def revert
    @post.revert!
    redirect_to :back
    flash[:notice] = "Post has been successfully reverted back to a draft state"
  end

  def revise
    @post.revise!
    redirect_to :back
    flash[:notice] = "Post Revision Request Sent"
  end

  def ban
    @post.ban!
    redirect_to :back
    flash[:notice] = "Post Banned Notice Sent"
  end

  def manage
    @drafts = @user.posts.drafts
    @pending_approval = @user.posts.pending_approval
    @approved = @user.posts.approved
    @needs_revision = @user.posts.needs_revision
    @banned = @user.posts.banned
  end

  def pass_off
    @revision = Revision.find(params[:id])
    @revision.send_to_user!
    redirect_to :back
  end

private

  def post_params
    params.require(:post).permit(:click_count,
                                 :favorited,
                                 :categories,
                                 :brief,
                                 :category_description,
                                 :post_picture,
                                 :title,
                                 :content,
                                 :workflow_state,
                                 :user_id,
                                 :category_id,
                                 :category_name,
                                 :tag_list,
    sources_attributes: [:id, :name, :url, :thumb, :description, :type])
  end

  def find_post
    @post = Post.find(params[:id])
  end

  def correct_user
    @user = current_user
  end

end
