class CommentsController < ApplicationController
  before_action :set_post
  before_action :set_comment, only: [:destroy]

def index
  @comments = Comment.all
end

def create
  @comment = @post.comments.create(comment_params)
  set_user
  if @comment.save
  else
    flash[:error] = "uh oh"
  end
  redirect_to @post
end

def destroy
  @comment.destroy
  redirect_to :back
end

private

  def set_comment
    @comment = Comment.find(params[:id])
  end

  def set_user
     @comment.user = current_user
  end

  def set_post
    @post = Post.find(params[:post_id])
  end

  def comment_params
    params.require(:comment).permit(:body, :user_id, :post_id)
  end
end
