class CollectionItemsController < ApplicationController
  before_action :set_collection_item, only: :destroy

  def create
    @collection_item = current_user.collection_items.create(collection_item_params)
    flash[:notice] = "Added to collection"
    redirect_to :back
  end

  def destroy
    @collection_item.destroy
    flash[:notice] = "Collection item was successfully deleted"
    redirect_to :back
  end

  def index
    @collection.collection_items
  end

  private

  def collection_item_params
    params.require(:collection_item).permit(:post_id, :notes, :collection_id, :user_id)
  end

  def set_collection_item
    @collection_item = @collection.collection_items.find(params[:id])
  end

end
