class StaticPagesController < ApplicationController


  def home
  end

  def welcome
  end

  def front_page
    @posts = Post.where(id: 6..100)
  end

  def analytics
  end

end
