class RankingsController < ApplicationController

  def index
  end

  def top_publishers
  end

  def top_articles
  end

  def top_categories
  end


end
