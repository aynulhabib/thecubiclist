class TaskListsController < ApplicationController
  before_action :set_task_list, only: [:destroy, :upvote, :downvote]

  def index
    @my_task_lists = current_user.task_lists
    @task_lists = TaskList.all
  end

  def create
    @task_list = current_user.task_lists.create(task_list_params)
    if @task_list.save
      flash[:success] = 'You successfully created a task list. Get shit done!'
      redirect_to :back
    else
      flash[:error] = 'There was an error'
      redirect_to :back
    end
  end

  def destroy
    @task_list = TaskList.destroy
    if @task_list.destroy
      flash[:success] = 'TaskList has been destroyed'
    else
      flash[:error] = 'There was an error'
    end
  end

  def upvote
    @task_list.upvote_by current_user

    if request.xhr?
      render json: { count: @task_list.get_likes.size, id: params[:id]}
    end
  end

  private

  def task_list_params
    params.require(:task_list).permit(:name,
                                      tasks_attributes: [:name,
                                                         :description,
                                                         :due_date,
                                                         :priority_level,
                                                         :reoccuring,
                                                         :frequency,
                                                         :complete,
                                                         :workflow_state,
                                                         :created_at,
                                                         :updated_at,
                                                         :taskable_id,
                                                         :taskable,
                                                         :reminder])
  end

  def set_task_list
    @task_list = TaskList.find(params[:id])
  end
end