class CardsController < ApplicationController
  before_action :set_card, except: [:new, :create, :index]

  def show
  end

  def index
    @cards = current_user.outbound_cards
  end

  def new
    @card = current_user.outbound_cards.new
  end

  def edit
  end

  def update
  end

  def create
    @card = current_user.outbound_cards.create(card_params)
    if @card.save
      flash[:success] = 'Card has been added to your cart, are you ready to check out?'
      redirect_to :back
    else
      flash[:error] = 'There was an error'
      redirect_to :back
    end
  end

  def destroy
    @card = Card.destroy
    if @card.destroy
      flash[:success] = 'Card has been destroyed'
    else
      flash[:error] = 'There was an error'
    end
  end

  def upvote
    @card.upvote_by current_user

    if request.xhr?
      render json: { count: @card.get_likes.size, id: params[:id]}
    end
  end

  def downvote
    @card.downvote_by current_user

    if request.xhr?
      render json: { count: @card.get_likes.size, id: params[:id]}
    end
  end



  private
  def card_params
    params.require(:card).permit(:name,
                                 :international,
                                 :price,
                                 :recipient_id,
                                 :sender_id,
                                 :workflow_state,
                                 :message,
                                 :front,
                                 :back,
                                 :template,
                                 :full_bleed,)
  end
    #address_attributes: [:address_line_1, :address_line_2, :address_city, :address_state, :address_zip, :address_country])
  def set_card
    @card = Card.find(params[:id])
  end
end