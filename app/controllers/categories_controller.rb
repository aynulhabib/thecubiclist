class CategoriesController < ApplicationController
  helper_method :sort_column, :sort_direction
  before_action :find_category, only: [:publish, :downvote, :upvote, :show, :edit, :update, :destroy]

  def new
  end

  def show
    @posts = @category.posts
  end

  def index
    respond_to do |format|
      format.html
      format.json { render json: @categories}
    end
    if params[:query].present?
      @categories = Category.search(params[:query], page: params[:page])
    else
      @categories = Category.order(sort_column + " " + sort_direction)
    end
  end

  def create
    @category = Category.create(category_params)
    if @category.save
      redirect_to :back
    else
      render 'new'
    end
  end

  def edit
  end

  def upvote
    @category.upvote_by current_user

    if request.xhr?
      render json: { count: @category.get_likes.size, id: params[:id]}
    end
  end

  def downvote
    @category.downvote_by current_user

    if request.xhr?
      render json: { count: @category.get_likes.size, id: params[:id]}
    end
  end

  def update
    if @category.update_attributes(category_params)
      redirect_to @category
    else
      render 'edit'
    end
  end

  def publish
    @category.publish!
    flash[:success]= "Category has been published successfully."
    redirect_to :back
  end

  def destroy
    if @category.destroy
      flash[:success] = "Category deleted"
      redirect_to categories_path
    end
  end

private
  def category_params
    params.require(:category).permit(:parent_id, :name, :description, :picture)
  end

  def find_category
    @category = Category.find(params[:id])
  end

  def sort_column
    Category.column_names.include?(params[:sort]) ? params[:sort] : "name"
  end

  def sort_direction
    %w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"
  end
end
