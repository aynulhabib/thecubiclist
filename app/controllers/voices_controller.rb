class VoicesController < ApplicationController
  before_action :set_voice, only: [:destroy, :upvote, :downvote]

  def index
    @my_voices = current_user.voices
    @voices = Voice.all
  end

  def create
    @voice = current_user.voices.create(voice_params)
    if @voice.save
      flash[:success] = 'Thanks! Your suggestion has been successfully added to the queue'
      redirect_to user_voices_path
    else
      flash[:error] = 'There was an error'
      redirect_to user_voices_path
    end
  end

  def destroy
    @voice = Voice.destroy
    if @voice.destroy
      flash[:success] = 'Voice has been destroyed'
    else
      flash[:error] = 'There was an error'
    end
  end

  def upvote
    @voice.upvote_by current_user

    if request.xhr?
      render json: { count: @voice.get_likes.size, id: params[:id]}
    end
  end

  def downvote
    @voice.downvote_by current_user

    if request.xhr?
      render json: { count: @voice.get_likes.size, id: params[:id]}
    end
  end

  private
  def voice_params
    params.require(:voice).permit(:title, :content)
  end

  def set_voice
    @voice = Voice.find(params[:id])
  end
end
