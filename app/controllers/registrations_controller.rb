class RegistrationsController < Devise::RegistrationsController

  def new
    super
  end

  def create
    super
  end

  def edit
  end

  def update
    if current_user.update_attributes(account_update_params)
      redirect_to user_path(@user)
      flash[:success] = "Profile has been successfully updated."
    else
      render 'edit'
    end
  end

  private
  def sign_up_params
    params.require(:user).permit(:email,
                                 :password,
                                 :password_confirmation,
                                 :profile_picture,
                                 :profile_picture_cache,
                                 :country,
                                 :pen_name,
                                 address_attributes: [:address_line_1,
                                                      :address_line_2,
                                                      :address_city,
                                                      :address_state,
                                                      :address_zip,
                                                      :address_country])
  end

  def account_update_params
    params.require(:user).permit(:email, :password, :password_confirmation, :profile_picture, :profile_picture_cache, :country, :bio, :cover_photo, :admin, :pen_name, :first_name, :last_name, :name)
  end


end