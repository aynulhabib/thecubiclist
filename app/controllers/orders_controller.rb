class OrdersController < ApplicationController


  def index
    @orders = current_user.orders
  end

  def create
    @order  = @current_user.orders.build()
  end

  def destroy
  end


  private

  def card_params
    params.require(:card).permit(:name,
                                 :international,
                                 :price,
                                 :recipient_id,
                                 :sender_id,
                                 :workflow_status,
                                 :message,
                                 :front,
                                 :back,
                                 :template,
                                 :full_bleed,
                                 address_attributes: [:address_line_1, :address_line_2, :address_city, :address_state, :address_zip, :address_country])
  end

  def set_card
    @card = Card.find(params[:id])
  end
end
