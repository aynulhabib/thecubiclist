class SubmissionsController < ApplicationController
  before_action :set_topic
  before_action :set_submission, only: [:show, :update, :destroy]

  def index
    @submissions = Submission.all
  end

  def create
    @submissions = @topic.submissions.create(submissions_params)
    set_user
    if @submissions.save
    else
      flash[:error] = "uh oh"
    end
    redirect_to @topic
  end

  def destroy
    @submission.destroy
    redirect_to :back
  end


  private

  def set_submissions
    @submission = Submission.find(params[:id])
  end

  def set_user
    @submission.user = current_user
  end

  def set_topic
    @topic = Topic.find(params[:topic_id])
  end

  def submissions_params
    params.require(:submission).permit(:body, :user_id, :topic_id)
  end
end
