class UsersController < ApplicationController
  before_action :find_user, only: [:show, :destroy, :toggle, :followers, :following]

  def index
    @feed_items = current_user.feed
    if params[:query].present?
      @users = User.search params[:query], suggest: true, page: params[:page], per_page: 5, fields: [:first_name, :last_name, :pen_name]
    else
      @users = User.all
    end
  end

  def show
    @posts = @user.posts
    @followers = @user.followers
    @following = @user.following
  end

  def destroy
      @user.destroy
      flash[:success] = "User deleted"
      redirect_to admin_users_path
  end

  def toggle
    @user.toggle!(:admin)

    if request.xhr?
      render json: { permission: @user.admin?, id: params[:id]}
      flash[:notice] = "Success! You changed #{@user.pen_name}'s admin privileges"
    end

  end

  def following
    @title = "Following"
    @users = @user.following.paginate(page: params[:page])
    render 'show_follow'
  end

  def followers
    @title = "Followers"
    @users = @user.followers.paginate(page: params[:page])
    render 'show_follow'
  end


private

  def find_user
    @user = User.find(params[:id])
  end



end


