class MessagesController < ApplicationController
  before_action :set_message, except: [:index, :create]

  def create
    @message = current_user.sent_messages.create(message_params)
    if @message.save
      flash[:success]= "Message has been sent"
      redirect_to :back
    else
      flash[:error] = "Something went wrong"
    end
  end

  def index
    @messages = current_user.incoming_messages
    @unread = current_user.incoming_messages.unread
    @read = current_user.incoming_messages.read
    @archived = current_user.incoming_messages.archived
    @sent = current_user.sent_messages
  end

  def destroy
    @message = Message.find(params[:id])
    @message.destroy
    redirect_to :back
    flash[:success]= "Message has been deleted"
  end

  def mark_as_read
    @message.mark_as_read!
    flash[:notice] = "message marked as read"
    redirect_to :back
  end

  def archive
    @message.archive!
    flash[:notice] = "message has been archived"
    redirect_to :back
  end

  def mark_as_new
    @message.mark_as_new!
    flash[:notice] = "message marked as read"
    redirect_to :back
  end

  private

  def set_message
    @message = Message.find(params[:id])
  end

  def message_params
    params.require(:message).permit(:subject, :body, :recipient_id)
  end
end
