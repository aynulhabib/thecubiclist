class FavoritesController < ApplicationController
  def index
    @favorites = current_user.get_voted Post
  end
end
