module ApplicationHelper
  include ActsAsTaggableOn::TagsHelper

  def resource_name
    :user
  end

  def resource
    @resource ||= User.new
  end

  def devise_mapping
    @devise_mapping ||= Devise.mappings[:user]
  end

  def time_and_date
    content_tag(:div, DateTime.now.strftime("%A, %B %d, %Y"))
  end

  def greeting
    t = Time.now.hour
    if t < 12
      greeting = "Good morning, kick today's ass."
    elsif t > 12 && t < 17
      greeting = "Ah yes, the old mid day-grind."
    elsif t > 17 && t < 19
      greeting = "I hope you got off at 5!"
    else
      greeting = "Hope you're having a great evening, #{current_user.pen_name}"
    end
  end

  def correct_user
    current_user.id == @user.id
  end

  def admin
    current_user.admin
  end

  def not_admin
    !current_user.admin
  end

  def sortable(column, title = nil)
    title ||= column.titleize
    css_class = column == sort_column ? "current #{sort_direction}" : nil
    direction = column == sort_column && sort_direction == "asc" ? "desc" : "asc"
    link_to title, {:sort => column, :direction => direction}, {:class => css_class}
  end

  def admin_tabs
    ["addresses","cards","orders","posts","categories","comments","sources","users","voices"]
  end

  def user_tabs
    return ["collection"]
  end



  def search_helper()
  end

  def day
    Time.now.strftime("%A")
  end

  def greeting
    RapGenius.search(day)
  end

  def stylesheet(stylesheet)
    content_for :stylesheet, stylesheet_link_tag(stylesheet)
  end

end
