module AdminHelper
  def resource_name
    :user
  end

  def resource
    @resource ||= User.new
  end

  def devise_mapping
    @devise_mapping ||= Devise.mappings[:user]
  end

  def time_and_date
    current_time = DateTime.now.strftime("%A, %B %d, %Y | %I:%M %p")
  end

  def greeting
    t = Time.now.hour
    if t < 12
      greeting = "Good morning, kick today's ass."
    elsif t > 12 && t < 17
      greeting = "Ah yes, the old mid day-grind."
    elsif t > 17 && t < 19
      greeting = "I hope you got off at 5!"
    else
      greeting = "Hope you're having a great evening, #{current_user.pen_name}"
    end
  end
end
