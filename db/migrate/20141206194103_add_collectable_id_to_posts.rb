class AddCollectableIdToPosts < ActiveRecord::Migration
  def change
    add_column :posts, :collectable_id, :integer
    add_column :posts, :collectable_type, :string
  end
end
