class CreateRevisionItems < ActiveRecord::Migration
  def change
    create_table :revision_items do |t|
      t.string :content
      t.string :workflow_state
      t.references :revision, index: true

      t.timestamps
    end
  end
end
