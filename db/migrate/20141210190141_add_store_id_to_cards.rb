class AddStoreIdToCards < ActiveRecord::Migration
  def change
    add_reference :cards, :store, index: true
  end
end
