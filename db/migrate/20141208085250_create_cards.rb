class CreateCards < ActiveRecord::Migration
  def change
    create_table :cards do |t|
      t.integer :sender_id
      t.string :workflow_status
      t.integer :recipient_id
      t.integer :price
      t.boolean :international

      t.timestamps
    end
  end
end
