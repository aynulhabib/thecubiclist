class RenameRelationships < ActiveRecord::Migration
  def change
    rename_column :relationships, :follower_id, :user_id
    rename_column :relationships, :followed_id, :friend_id
  end
end
