class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.string :title
      t.text :brief
      t.text :content
      t.string :picture
      t.string :workflow_state
      t.references :category, index: true
      t.references :user, index: true

      t.timestamps
    end
  end
end
