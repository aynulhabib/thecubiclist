class AddPostIdToRevisionItems < ActiveRecord::Migration
  def change
    add_reference :revision_items, :post, index: true
  end
end
