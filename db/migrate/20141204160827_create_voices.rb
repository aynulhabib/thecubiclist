class CreateVoices < ActiveRecord::Migration
  def change
    create_table :voices do |t|
      t.string :content
      t.references :user, index: true

      t.timestamps
    end
  end
end
