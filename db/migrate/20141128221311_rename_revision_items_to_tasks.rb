class RenameRevisionItemsToTasks < ActiveRecord::Migration
  def change
    rename_table :revision_items, :tasks
  end
end
