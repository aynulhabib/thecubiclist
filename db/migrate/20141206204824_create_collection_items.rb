class CreateCollectionItems < ActiveRecord::Migration
  def change
    create_table :collection_items do |t|
      t.references :post, index: true
      t.text :notes

      t.timestamps
    end
  end
end
