class CreateTasklists < ActiveRecord::Migration
  def change
    create_table :tasklists do |t|
      t.references :user, index: true

      t.timestamps
    end
  end
end
