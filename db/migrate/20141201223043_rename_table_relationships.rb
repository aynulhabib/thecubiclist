class RenameTableRelationships < ActiveRecord::Migration
  def change
    rename_table :relationships, :friendships
  end
end
