class AddSubcategoryIdToPosts < ActiveRecord::Migration
  def change
    add_reference :posts, :sub_category, index: true
    add_reference :posts, :parent_category, index: true
  end
end
