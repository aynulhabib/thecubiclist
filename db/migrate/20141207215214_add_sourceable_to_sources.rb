class AddSourceableToSources < ActiveRecord::Migration
  change_table :sources do |t|
    t.references :sourceable, :polymorphic => true
  end
end
