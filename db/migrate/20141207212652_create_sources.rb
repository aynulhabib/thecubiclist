class CreateSources < ActiveRecord::Migration
  def change
    create_table :sources do |t|
      t.string :name
      t.string :url
      t.string :thumb
      t.string :description
      t.string :type

      t.timestamps
    end
  end
end
