class AddColumnsToTasks < ActiveRecord::Migration
  change_table :tasks do |t|
    t.references :taskable, :polymorphic => true
  end
end
