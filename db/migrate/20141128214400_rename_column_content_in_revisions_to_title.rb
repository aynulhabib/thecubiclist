class RenameColumnContentInRevisionsToTitle < ActiveRecord::Migration
  def change
    rename_column :revisions, :content, :title
  end

end
