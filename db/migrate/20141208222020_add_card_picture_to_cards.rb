class AddCardPictureToCards < ActiveRecord::Migration
  def change
    add_column :cards, :front, :string
    add_column :cards, :back, :string
    add_column :cards, :template, :boolean
    add_column :cards, :full_bleed, :boolean
  end
end
