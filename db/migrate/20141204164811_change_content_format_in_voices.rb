class ChangeContentFormatInVoices < ActiveRecord::Migration
  def change
    change_column :voices, :content, :text
  end
end
