class AddFieldsToTasks < ActiveRecord::Migration
  def change
    add_column :tasks, :reoccuring, :boolean
    add_column :tasks, :priority_level, :integer
    add_column :tasks, :name, :string
    add_column :tasks, :due_date, :datetime
    add_column :tasks, :reminder, :datetime
  end
end
