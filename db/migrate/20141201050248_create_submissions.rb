class CreateSubmissions < ActiveRecord::Migration
  def change
    create_table :submissions do |t|
      t.text :content
      t.references :user, index: true
      t.references :topic, index: true

      t.timestamps
    end
  end
end
