class AddUserIdToRevisions < ActiveRecord::Migration
  def change
    add_reference :revisions, :user, index: true
  end
end
