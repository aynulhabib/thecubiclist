class RemoveColumnFromTasks < ActiveRecord::Migration
  def change
    remove_reference :tasks, :polymorphic, index: true
  end
end
