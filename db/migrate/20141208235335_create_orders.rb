class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.integer :order_code
      t.references :orderable, polymorphic: true
      t.timestamps
    end
  end
end
