class CreateClicks < ActiveRecord::Migration
  def change
    create_table :clicks do |t|
      t.references :user, index: true
      t.references :post, index: true
      t.string :ip
      t.string :url

      t.timestamps
    end
  end
end
