class AddContestIdToUsers < ActiveRecord::Migration
  def change
    add_reference :users, :contest, index: true
  end
end
