class CreateRevisions < ActiveRecord::Migration
  def change
    create_table :revisions do |t|
      t.string :content
      t.references :post, index: true

      t.timestamps
    end
  end
end
