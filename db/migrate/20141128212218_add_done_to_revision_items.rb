class AddDoneToRevisionItems < ActiveRecord::Migration
  def change
    add_column :revision_items, :done, :boolean
  end
end
