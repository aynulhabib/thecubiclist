class RemoveColumnsFromCategories < ActiveRecord::Migration
  def up
    remove_column :categories, :lft
    remove_column :categories, :rgt
  end

  def down
    add_column :categories, :lft
    add_column :categories, :rgt
  end
end
