class AddCollectionToCollectionItems < ActiveRecord::Migration
  def change
    add_reference :collection_items, :collection, index: true
  end
end
