class AddReminderToTasks < ActiveRecord::Migration
  def change
    add_column :tasks, :reminder, :datetime
  end
end
