class AddNameToTasklists < ActiveRecord::Migration
  def change
    add_column :tasklists, :name, :string
  end
end
