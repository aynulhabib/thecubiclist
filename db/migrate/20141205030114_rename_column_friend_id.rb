class RenameColumnFriendId < ActiveRecord::Migration
  def change
    rename_column :relationships, :friend_id, :follower_id
    add_column :relationships, :followed_id, :integer
  end
end
