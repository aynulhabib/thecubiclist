class AddPenNameAndProfilePictureToUsers < ActiveRecord::Migration
  def change
    add_column :users, :pen_name, :string
    add_column :users, :profile_picture, :string
  end
end
