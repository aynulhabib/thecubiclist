class AddTitleToVoices < ActiveRecord::Migration
  def change
    add_column :voices, :title, :string
  end
end
