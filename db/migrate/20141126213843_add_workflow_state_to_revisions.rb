class AddWorkflowStateToRevisions < ActiveRecord::Migration
  def change
    add_column :revisions, :workflow_state, :string
  end
end
