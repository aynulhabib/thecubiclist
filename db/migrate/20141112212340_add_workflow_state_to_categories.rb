class AddWorkflowStateToCategories < ActiveRecord::Migration
  def change
    add_column :categories, :workflow_state, :text
  end
end
