class CreateInvitations < ActiveRecord::Migration
  def change
    create_table :invitations do |t|
      t.string :email
      t.string :first_name
      t.string :last_name
      t.string :subject
      t.text :body

      t.timestamps
    end
  end
end
