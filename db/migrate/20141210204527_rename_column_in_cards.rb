class RenameColumnInCards < ActiveRecord::Migration
  def change
    rename_column :cards, :workflow_status, :workflow_state
  end
end
