class AddFavoritedToPosts < ActiveRecord::Migration
  def change
    add_column :posts, :favorited, :boolean
  end
end
