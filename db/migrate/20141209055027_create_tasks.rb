class CreateTasks < ActiveRecord::Migration
  def change
    create_table :tasks do |t|
      t.string :name
      t.text :description
      t.datetime :due_date
      t.integer :priority_level
      t.boolean :reoccuring
      t.string :frequency
      t.boolean :complete
      t.string :workflow_state
      t.references :polymorphic, index: true

      t.timestamps
    end
  end
end
