class ChangeTableNameFromFriendshipsToRelationships < ActiveRecord::Migration
  def change
    rename_table :friendships, :relationships
  end
end
